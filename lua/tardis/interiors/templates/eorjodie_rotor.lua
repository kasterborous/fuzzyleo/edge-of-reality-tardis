TARDIS:AddInteriorTemplate("eorjodie_defaultclaw", {
	Interior = {

		Parts={
			eorjodie_claw1            =   {ang = Angle(0, 0, 0),},
			eorjodie_claw2            =   {ang = Angle(0, 60, 0),},
			eorjodie_claw3            =   {ang = Angle(0, 120, 0),},
			eorjodie_claw4            =   {ang = Angle(0, 180, 0),},
			eorjodie_claw5            =   {ang = Angle(0, -60, 0),},
			eorjodie_claw6            =   {ang = Angle(0, -120, 0),},
		},
	},
})

TARDIS:AddInteriorTemplate("eorjodie_frozenclaw", {
	Interior = {

		Parts={
			eorjodie_pillarstatictop1            =   {ang = Angle(0, 0, 0),},
			eorjodie_pillarstatictop2            =   {ang = Angle(0, 60, 0),},
			eorjodie_pillarstatictop3            =   {ang = Angle(0, 120, 0),},
			eorjodie_pillarstatictop4            =   {ang = Angle(0, 180, 0),},
			eorjodie_pillarstatictop5            =   {ang = Angle(0, -60, 0),},
			eorjodie_pillarstatictop6            =   {ang = Angle(0, -120, 0),},
		},
	},
})

TARDIS:AddInteriorTemplate("eorjodie_accurateclaw", {
	Interior = {

		Parts={
			eorjodie_altclaw1            =   {ang = Angle(0, 0, 0),},
			eorjodie_altclaw2            =   {ang = Angle(0, 60, 0),},
			eorjodie_altclaw3            =   {ang = Angle(0, 120, 0),},
			eorjodie_altclaw4            =   {ang = Angle(0, 180, 0),},
			eorjodie_altclaw5            =   {ang = Angle(0, -60, 0),},
			eorjodie_altclaw6            =   {ang = Angle(0, -120, 0),},
		},
	},
})

TARDIS:AddInteriorTemplate("eorjodie_accuraterotor", {
	Interior = {

		Parts={
			eorjodie_rotor            =   {ang = Angle(0, 120, 0),},
		},
	},
})