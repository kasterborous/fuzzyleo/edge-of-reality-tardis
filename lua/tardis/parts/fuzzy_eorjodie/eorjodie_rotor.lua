-- adds rotor stuff

-------------
local PART={}
PART.ID = "eorjodie_rotor"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/rotor.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw3"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw4"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw5"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_claw6"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartop.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw3"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw4"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw5"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_altclaw6"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopreverse.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 0.3, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = true,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_smalltardis"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/bluetardis.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 10, -- you can change the value
    Type = "travel", -- this will make the rotor work
    ReturnAfterStop = false,
    NoPowerFreeze = true,
}

TARDIS:AddPart(PART)

