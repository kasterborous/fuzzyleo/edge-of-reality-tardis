-- adds animated stuff

-------------
local PART={}
PART.ID = "eorjodie_switch1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/switch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_EggTimerTurnClick_Loop2.mp3"
PART.AnimateOptions = {
    Speed = 2, -- you can change the value
}

TARDIS:AddPart(PART)

local PART={}
PART.ID = "eorjodie_biglever"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/biglever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_LeverMain1.mp3"
PART.AnimateOptions = {
    Speed = 3, -- you can change the value
}
TARDIS:AddPart(PART)

local PART={}
PART.ID = "eorjodie_biglever2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/biglever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.Sound = "FuzzyLeo/EdgeOfReality/BBC_TARDIS_LeverMain1.mp3"
PART.AnimateOptions = {
    Speed = 3, -- you can change the value
}
TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_switch2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/switch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateOptions = {
    Speed = 1, -- you can change the value
}

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_manualdestination"
PART.Model = "models/FuzzyLeo/ButtonHitbox.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

