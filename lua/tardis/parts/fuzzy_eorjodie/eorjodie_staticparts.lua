-- adds all static parts

-------------
local PART={}
PART.ID = "eorjodie_domevoid"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/domevoid.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_console"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/console.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_consoledetail1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/consoledetail1.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_consoledetail2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/consoledetail2.mdl"
PART.AutoSetup = true
PART.UseTransparencyFix = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_interiorpb"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/interiorpb.mdl"
PART.AutoSetup = true
PART.Collision = true

if SERVER then
	hook.Add("SkinChanged", "eorjodie-interiorpb", function(ent,i)
		if ent.TardisExterior then
			local eorjodie_interiorpb=ent:GetPart("eorjodie_interiorpb")
			if IsValid(eorjodie_interiorpb) then
				eorjodie_interiorpb:SetSkin(i)
			end
			if IsValid(ent.interior) then
				local eorjodie_interiorpb=ent.interior:GetPart("eorjodie_interiorpb")
				if IsValid(eorjodie_interiorpb) then
					eorjodie_interiorpb:SetSkin(i)
				end
			end
		end
	end)
end


TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_interiorwalls"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/interiorwalls.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_roundels"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/roundels.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar3"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar4"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar5"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillar6"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillar.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop1"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop2"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop3"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop4"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop5"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_pillarstatictop6"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/pillartopstatic.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_interiorwallfunnel"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/interiordoorfunnel.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)

-------------
local PART={}
PART.ID = "eorjodie_classicwallfunnel"
PART.Model = "models/FuzzyLeo/EdgeOfRealityJodie/classicdoorfunnel.mdl"
PART.AutoSetup = true
PART.Collision = true

TARDIS:AddPart(PART)